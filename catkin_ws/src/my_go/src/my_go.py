#!/usr/bin/env python

import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Point, Twist, Pose2D
from math import atan2
 
x = 0.0
y = 0.0 
theta = 0.0
goal = Pose2D()
speed = Twist()
goal.x = 0.0
goal.y = 0.0

 
def newOdom(msg):
    global x
    global y
    global theta
 
    x = msg.pose.pose.position.x
    y = msg.pose.pose.position.y
 
    rot_q = msg.pose.pose.orientation
    (roll, pitch, theta) = euler_from_quaternion([rot_q.x, rot_q.y, rot_q.z, rot_q.w])

def newpose(msg):
    global goal
    goal.x = msg.x
    goal.y = msg.y

def my_go():
    global goal
    inc_x = goal.x - x
    inc_y = goal.y - y
 
    angle_to_goal = atan2(inc_y, inc_x)
    if (angle_to_goal > 0 and theta > 0) or (angle_to_goal < 0 and theta < 0):
    	    delta_angle = angle_to_goal - theta
    else:
            delta_angle = angle_to_goal + theta
    
    if abs(inc_x) > 0.05 or abs(inc_y) > 0.05:
	    if delta_angle > 0.1:
		speed.linear.x = 0.0
		speed.angular.z = 0.9
	    elif delta_angle < -0.1:
		speed.linear.x = 0.0
		speed.angular.z = -0.9
	    else:
		speed.linear.x = 0.5
		speed.angular.z = 0.0
            goal.theta = 0.0 
    else:
	    speed.linear.x = 0.0
	    speed.angular.z = 0.0
    	    goal.theta = 1.0       
    pub.publish(speed)
    pub2.publish(goal)
    r.sleep()  
 
rospy.init_node("my_go")
 
sub = rospy.Subscriber("/odom", Odometry, newOdom)
pub = rospy.Publisher("/mobile_base/commands/velocity", Twist, queue_size = 1)
sub2 = rospy.Subscriber("/my_go", Pose2D, newpose)
pub2 = rospy.Publisher("/my_go", Pose2D, queue_size = 1)
 
r = rospy.Rate(30)
 
while not rospy.is_shutdown():
      my_go()
