#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Pose2D

response = 0.0
goal = Pose2D()

def callback(msg):
	global response
	response = msg.theta

def point_go(x, y):
	global response
	goal.x = x
	goal.y = y
	goal.theta = 0.0
	pub.publish(goal)
	rospy.sleep(0.5)
	while response != 1.0:
		pass
	r.sleep()

def stop():
	while 1 != 0:
		pass

rospy.init_node("points")

sub = rospy.Subscriber("/my_go", Pose2D, callback)
pub = rospy.Publisher("/my_go", Pose2D, queue_size = 1)

r = rospy.Rate(30)
rospy.sleep(1.0)

while not rospy.is_shutdown():
	point_go(-0.23, 0)
	point_go(-0.23, 6.28)
	point_go(-3.2, 6.28)
	point_go(-4.87, 5.43)
	point_go(-6.0, 5.43)
	point_go(-6.0, 6.13)
	point_go(-6.0, 8.34)
	point_go(-6.9, 8.34)
	stop()
